module.exports = {
  dialect: 'postgres',
  host: 'localhost',
  username: 'postgres',
  password: 'docker',
  database: 'teclat',
  define: {
    timestamps: true,
    underscored: true,
    underscoredAll: true,
  },
};
