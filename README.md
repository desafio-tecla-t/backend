# Insomnia Settings

* Base Evironment do Insomnia
```
{
  "base_url": "http://localhost:8080",
  "token": " 'token gerado pela Session' "
}
```
## Rotas

### Users
##### Create User 
* POST {{ base_url  }}/users
```
{
	"name": "User Name",
	"email": "user@email.com",
	"password": "userpassword"
}
```
##### Update User by id
* PUT {{ base_url  }}/users/:id

##### List 
* GET {{ base_url  }}/users

##### Delete User by id
* DELETE {{ base_url  }}/user/:id

##### Get User by id
* GET {{ base_url }}/users/:id


### Session
##### Create Session
* POST {{ base_url }}/session
```
{
	"email": "user@email.com",
	"password": "userpassword"
}
```

### Files
#### Create File
* POST {{ base_url }}/files
Multipart Form 
name:File
value:File

# How to Start

## Docker 

### Postgres

``$ docker run --name database -e POSTGRES_PASSWORD=docker -p 5432:5432 -d postgres:11``

## Sequelize

``$ yarn sequelize db:migrate``

## App
``$ yarn``
``$ yarn dev``